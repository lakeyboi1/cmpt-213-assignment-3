package cmpt213as3.ui;
import java.util.ArrayList;
import cmpt213as3.model.Cell;

public class DisplayBoard {
    private int numRows;
    private int numColumns;
    private final String SPACE = "   ";

    public DisplayBoard (int numRows, int numColumns) {
        this.numRows = numRows;
        this.numColumns = numColumns;
    }

    public void displayBoard (ArrayList<Cell> cells, boolean end, int fortressHealth) {
        System.out.println("Game Board:");
        System.out.print(" " + SPACE);
        for (int i = 1; i <= numColumns; i++) {
            System.out.print(i + SPACE);
        }

        char currentRow = ' ';
        for (Cell cell : cells) {
            if (cell.retrieveCellRow() != currentRow) {
                currentRow = cell.retrieveCellRow();
                System.out.print("\n" + currentRow + SPACE);
            }

            if (end) {
                System.out.print(cell.gameEndCells() + SPACE);
            } else {
                System.out.print(cell.retrieveCellStatus() + SPACE);
            }
        }
        System.out.println("\nFortress Structure Left: " + fortressHealth);
    }

    public void displayIfShotHit (boolean isTank) {
        if (isTank) {
            System.out.println("Hit!\n");
        } else {
            System.out.println("Miss.\n");
        }
    }

    public void displayTankDamage (int tankNumber, int tanksAlive, int damage) {
        System.out.println("Alive tank #" + tankNumber + " of " + tanksAlive + " hit you for " + damage + " damage!");
    }

    public void displayEnd (ArrayList<Cell> cells, boolean win) {
        if (win) {
            System.out.println("Congratulations! You won!");
        } else {
            System.out.println("Sorry, your fortress has been destroyed.");
        }
        displayBoard(cells, true, 0);
    }
}
