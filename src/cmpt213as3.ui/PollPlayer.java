package cmpt213as3.ui;
import java.util.*;

public class PollPlayer {
    private ArrayList<String> validMoves = new ArrayList<>();

    public PollPlayer (int numRows, int numColumns) {
        for (char i = 'a'; i < 'a' + numRows; i++) {
            for (int j = 1; j <= numColumns; j++) {
                validMoves.add(Character.toString(i) + j);
            }
        }
    }

    public String getPlayerMove () {
        String playerMove = "";
        Scanner input = new Scanner(System.in);

        System.out.print("Enter which cell you want to hit: ");
        playerMove = input.nextLine().toLowerCase();

        while (!validMoves.contains(playerMove)) {
            System.out.println("Please enter a valid cell.");
            playerMove = input.nextLine().toLowerCase();
        }
        return playerMove;
    }
}