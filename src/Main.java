import cmpt213as3.model.GameBoard;
import cmpt213as3.ui.PollPlayer;

// Don't forget to import all the classes you will test!

public class Main {
    static final int NUM_ROWS = 10;
    static final int NUM_COLUMNS = 10;
    static final int TANK_SIZE = 5;
    static int NUM_TANKS = 5;

    public static void main (String[] args) {
        if (args.length != 0) {
            NUM_TANKS = Integer.parseInt(args[0]);
        }

        PollPlayer getInfo = new PollPlayer(NUM_ROWS, NUM_COLUMNS);
        GameBoard playBoard = new GameBoard(NUM_ROWS, NUM_COLUMNS, NUM_TANKS, TANK_SIZE);
        String currentPlayerMove = "";

        while (!playBoard.gameEnded()) {
            playBoard.displayBoard();
            currentPlayerMove = getInfo.getPlayerMove();
            playBoard.processTurn(currentPlayerMove);
        }
        playBoard.displayEnd();
    }
}
