package cmpt213as3.model;
import java.util.*;

public class TankShapeGenerator {
    static final int NUM_DIRECTIONS = 3;
    static int VERTICAL_MOVE;
    static final int HORIZONTAL_MOVE = 1;
    int maxIndexOfGameBoard;
    ArrayList<ArrayList<Integer>> allTanks = new ArrayList<>();

    public TankShapeGenerator(int numTanks, int tankSize, int numColumns , int maxIndexOfGameBoard) {
        this.maxIndexOfGameBoard = maxIndexOfGameBoard;
        this.VERTICAL_MOVE = numColumns;

        for (int i = 0; i < numTanks; i++) {
            createShape(tankSize);
        }
    }

    private boolean checkForIllegalPosition (int indexOfNewCell, ArrayList<Integer> newTank) {
        if (newTank.contains(indexOfNewCell) || indexOfNewCell >= maxIndexOfGameBoard || indexOfNewCell < 0) {
            return true;
        }

        for (ArrayList<Integer> tank : allTanks) {
            if (tank.contains(indexOfNewCell)) {
                return true;
            }
        }

        return false;
    }

    private void createShape (int tankSize) {
        ArrayList<Cell> tankCells = new ArrayList<>();
        ArrayList<Integer> newTank = new ArrayList<>();;

        Random rng = new Random();
        int newTankBaseIndex = rng.nextInt(maxIndexOfGameBoard);
        while (checkForIllegalPosition(newTankBaseIndex, newTank)) {
            newTankBaseIndex = rng.nextInt(maxIndexOfGameBoard);
        }
        newTank.add(newTankBaseIndex);

        // Current index is manipulated within the switch statement to add new cells
        int currentIndex = newTankBaseIndex;

        for (int i = newTank.size(); i < tankSize; i++) {
            do {
                currentIndex = newTank.get(rng.nextInt(newTank.size()));
                switch(rng.nextInt(NUM_DIRECTIONS)) {
                    case 0:
                        currentIndex -= VERTICAL_MOVE;
                        break;
                    case 1:
                        if (currentIndex % VERTICAL_MOVE != 0) {
                            currentIndex -= HORIZONTAL_MOVE;
                        }
                        break;
                    case 2:
                        currentIndex += VERTICAL_MOVE;
                        break;
                    case 3:
                        if (currentIndex % VERTICAL_MOVE != VERTICAL_MOVE - 1) {
                            currentIndex += HORIZONTAL_MOVE;
                        }
                        break;
                    default:
                        System.out.println("Error in TankGenerator.createShape");
                        assert(false);
                }
            } while (checkForIllegalPosition(currentIndex, newTank));
            newTank.add(currentIndex);
        }
        allTanks.add(newTank);
    }

    public ArrayList<ArrayList<Integer>> getTanks () {
        return allTanks;
    }
}
