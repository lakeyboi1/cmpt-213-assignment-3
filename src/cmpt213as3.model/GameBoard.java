package cmpt213as3.model;
import java.util.ArrayList;
import java.util.Objects;

import cmpt213as3.ui.DisplayBoard;

public class GameBoard {
    private ArrayList<Cell> cells = new ArrayList<>();
    private ArrayList<Tank> tanks = new ArrayList<>();
    private Fortress playerFort = new Fortress(2500);

    private DisplayBoard boardOutput;

    public GameBoard (int numRows, int numColumns, int numTanks, int tankSize) {
        boardOutput = new DisplayBoard(numRows, numColumns);

        for (char i = 'a'; i < 'a' + numRows; i++) {
            for (int j = 1; j <= numColumns; j++) {
                cells.add(new Cell(Character.toString(i) + j));
            }
        }

        TankShapeGenerator gen = new TankShapeGenerator(numTanks, tankSize, numColumns, cells.size() - 1);
        ArrayList<ArrayList<Integer>> allTanks = gen.getTanks();

        for (int j = 0; j < allTanks.size(); j++) {
            tanks.add(new Tank((char)('A' + j), tankSize));

            for (Integer index : allTanks.get(j)) {
                cells.get(index).setTank((char)('A' + j));
            }
        }
    }

    public void displayBoard () {
        boardOutput.displayBoard(cells, false, playerFort.getFortressHealth());
    }

    public void processTurn (String playerMove) {
        for (Cell cell : cells) {
            if (Objects.equals(cell.getCellName(), playerMove)) {
                cell.shootCell();

                for (Tank tank : tanks) {
                    if (tank.getTankLetter() == cell.getTankLetter()) {
                        tank.tankHit();
                        break;
                    }
                }
                boardOutput.displayIfShotHit(cell.isTank());
                break;
            }
        }

        int totalTurnDamage = 0;
        int aliveTanks = 0;

        for (Tank tank : tanks) {
            if (!tank.dead()) {
                aliveTanks++;
            }
        }

        for (int i = 0; i < tanks.size(); i++) {
            if (!tanks.get(i).dead()) {
                totalTurnDamage += tanks.get(i).attack();
                boardOutput.displayTankDamage(i + 1, aliveTanks, tanks.get(i).attack());
            }
        }
        playerFort.takeDamage(totalTurnDamage);
    }

    public boolean gameEnded () {
        if (playerFort.getFortressHealth() == 0) {
            return true;
        }

        for (Tank tank : tanks) {
            if (!tank.dead()) {
                return false;
            }
        }
        return true;
    }

    public void displayEnd () {
        if (playerFort.getFortressHealth() > 0) {
            boardOutput.displayEnd(cells,true);
        } else {
            boardOutput.displayEnd(cells,false);
        }
    }
}
