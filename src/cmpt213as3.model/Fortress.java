package cmpt213as3.model;

public class Fortress {
    private int fortressHealth;

    public Fortress (int fortressHealth) {
        this.fortressHealth = fortressHealth;
    }

    public int getFortressHealth () {
        if (fortressHealth > 0) {
            return fortressHealth;
        }
        return 0;
    }

    public void takeDamage (int damage) {
        fortressHealth -= damage;
    }
}

