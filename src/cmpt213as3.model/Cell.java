package cmpt213as3.model;

public class Cell {
    private String cellName = "";
    private boolean isShot = false;
    private boolean isTank = false;
    private char tankLetter;

    public Cell (String cellName) {
        this.cellName = cellName;
    }

    public void setTank(char tankLetter) {
        isTank = true;
        this.tankLetter = tankLetter;
    }

    public void shootCell () {
        isShot = true;
    }

    public boolean isTank () {
        return isTank;
    }

    public char retrieveCellStatus () {
        if (isShot && !isTank) {
            return ' ';
        } else if (isShot && isTank) {
            return 'X';
        }
        return '~';
    }

    public char gameEndCells () {
        if (isShot && !isTank) {
            return ' ';
        } else if (isShot && isTank) {
            return Character.toLowerCase(tankLetter);
        } else if (isTank) {
            return tankLetter;
        }
        return '.';
    }

    public char retrieveCellRow () {
        return Character.toUpperCase(cellName.charAt(0));
    }

    public String getCellName () {
        return cellName;
    }

    public char getTankLetter () {
        return tankLetter;
    }
}
