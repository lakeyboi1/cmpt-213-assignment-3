package cmpt213as3.model;
import java.util.ArrayList;

public class Tank {
    private final int[] damageList = {0, 1, 2, 5, 20, 20};
    private int tankCellsRemaining = 5;
    private char tankLetter;

    public Tank (char tankLetter, int tankSize) {
        this.tankLetter = tankLetter;
        tankCellsRemaining = tankSize;
    }

    public char getTankLetter () {
        return tankLetter;
    }

    public int attack () {
        return damageList[tankCellsRemaining];
    }

    public void tankHit () {
        tankCellsRemaining--;
    }

    public boolean dead() {
        return tankCellsRemaining == 0;
    }
}
